# README #

--- utils/compareDetFiles 
--- Version 1.01
--- by Konstantin Sharko (Tomsk Polytechnic University)


**Description**

The program is deigned for presentations of results of alignment procedure in COMPASS CERN experiment. Allows to compare coordinates of given detector in several geometry files. Creates a table with number of alignment iteration in the first column and coordinates of given detector from respective geomeytry file in the second column.


**Compatibility**

Tested with ghc 7.10.3


**Building of execution file**

$ ghc compareDetectors.hs


**Execution**

./compareDetFiles "<prefix>" "<iterations>" "<suffix>" "<detector>" "<coordinate>"
<prefix> - the part of geometry file name which is before the number of iteration
<iteration> - last iteration number of alignment
<suffix> - the part of geometry file name which is after the nimber of iteration
<detector> - name of detector
<coordinate> - coordinate of detector center


**Execution without building**

$ runhaskell compareDetFiles.hs "<prefix>" "<iterations>" "<suffix>" "<detector>" "<coordinate>"


**Format of output**

<detector>_<coordinate>.out


--- Example

Input files: 
detectors.aloff.ai0.dat, detectors.aloff.ai1.dat, detectors.aloff.ai2.dat. *

Execution:
$ ./compareDetectors "detectors.aloff.ai" "2" ".dat" "FI01X1" "y"

Output file:
FI01X1_y.out

* Pay attention that numeration of input files should start from 0.