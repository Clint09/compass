#!/usr/bin/python

def left_part( string, value, column, positions ):
    return string[ 0 : ( positions[ column ] - len( value ) ) ]

def right_part( string, value, column, positions ):
    return (string[ positions[ column ] : 163 ] + '\n' )

def find_value( column, rf, mf_obj, mf_TBname, mf_det, obj, TBname, det):
    for rf_line in rf:
        rf_list = rf_line.split()
        if rf_list == []:
            continue
        if rf_list[obj] == mf_obj and rf_list[TBname] == mf_TBname and rf_list[det] == mf_det:
            return rf_list[ column ]
    return 'NONE'

def find_det(mf_line ):
    mf_list = mf_line.split()
    
###
v = ( 0,   1,  2,      3,   4,    5,  6,     7,     8,     9,     10,   11,   12,   13,       14,    15)
(     obj, ID, TBname, det, unit, sh, size1, size2, size3, size4, Zcen, Xcen, Ycen, rot_matr, pitch, angle ) = v  #columns of det.dat
p = ( 4,   9,  19,     25,  28,   33, 42,    50,    58,    67,    79,   90,   101,  106,      118,   127 ) #number of last symbol of value in string of det.dat

### compared parameter ###
comp_par = angle

### name of det.dat files ###
writefile = 'detectors.270682.OFF.mu-.new.dat'  #output file
mutablefile = 'detectors.270682.OFF.mu-.dat'  #input file
referencefile = 'detectors.2015.mc.dat'  #nominal file

wf = open( writefile, 'w' )
mf = open( mutablefile, 'r' )

for mf_line in mf:
    mf_list = mf_line.split()
    if mf_list == [] or mf_list[obj] != 'det':
        wf.write( mf_line )
        continue
    rf = open( referencefile, 'r' )
    value = find_value( comp_par, rf, mf_list[obj], mf_list[TBname], mf_list[det], obj, TBname, det)
    rf.close()
    if value == 'NONE':
        wf.write( mf_line )
        continue
    wf.write( left_part( mf_line, value, comp_par, p ) + value + right_part ( mf_line, value, comp_par, p ) )

wf.close()
mf.close()
