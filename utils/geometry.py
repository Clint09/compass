def parse_geometry( source,  # file or tuple of lines
                    prefixes,  # tuple of strings denoting first token in interesting lines
                    keepAuxInfo=False  # Wheter do keep non-prefixed information
                   ):
    # source --- iterable entity, so:
    for line in source:
        if tokenize_line( line )[0] in prefixes:
            # ... append result
        elif keepAuxInfo:
            # ... append aux info in result
        else:
            pass  # do nothing with this line
    # ... TODO
    return result

# Where result, for prefixes = ['foo', 'bar', ...] should be:
result = {
        "foo" : [[ 1.23, 4.56, ... ],
                  [ 7.89, 0.12, ... ],
                  ...
                  ],
        "bar" : [[ 1.23, 4.56, ... ],
                  [ 7.89, 0.12, ... ],
                  ...
                  ],
        ...
        # and, if keepAuxInfo was True:
        "@auxinfo" : [ {
            "lineno" : 0,
            "content" : "//$author: Yann Bedfer" },
            ...
        ]
    }
