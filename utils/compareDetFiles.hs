import System.Environment
import Data.List
import Data.Char
import Data.Maybe
import System.IO


main = do
    args <- getArgs		-- prefix, i, suffix, detName, crd
    if (checkArgs args)
        then putStrLn "args are OK"
        else putStrLn "args should be: \"prefix\" \"iterations\" \"suffix\" \"detName\" \"coordinate\"; i.e.: \"detectors.aloff.ai\" \"7\" \".dat\" \"DC01X1\" \"x\""
    let prefix = (args !! 0)
        i = read (args !! 1)
        suffix = (args !! 2)
        detName = (args !! 3)
        crd = head (args !! 4)
    output <- openFile (detName ++ "_" ++ [crd] ++ ".out") WriteMode
    writeData prefix i suffix detName crd output
    hClose output
    putStrLn ("output is in the \"" ++ detName ++ "_" ++ crd ++ ".out\"")
    return ()


checkArgs :: [String] -> Bool
checkArgs args = ( bool1 && bool2 )
    where bool1 = and $ map isNumber (args !! 1)
          bool2 = length args == 5


openDetFileI :: String -> Int -> String -> IO Handle
openDetFileI prefix i suffix = openFile (prefix ++ (show i) ++ suffix) ReadMode


findInFile :: Handle -> String -> Char -> Handle -> IO Double
findInFile detFile detName crd output = do
    str <- hGetLine detFile
    if (getDetParFromStr detName crd str == "")
        then findInFile detFile detName crd output
        else return (read (getDetParFromStr detName crd str)::Double)
	

writeData :: String -> Int -> String -> String -> Char -> Handle -> IO ()	
writeData prefix 0 suffix detName crd output = do
    detFile <- openDetFileI prefix 0 suffix
    value <- findInFile detFile detName crd output
    hPutStr output ("0 " ++ (show value) ++ " 0")
    hClose detFile
writeData prefix i suffix detName crd output = do
    detFile <- openDetFileI prefix i suffix
    value1 <- findInFile detFile detName crd output
    hPutStr output ((show i) ++ " " ++ (show value1) )
    hClose detFile
    detFile <- openDetFileI prefix (i-1) suffix
    value2 <- findInFile detFile detName crd output
    hPutStrLn output (" " ++ (show $ value1 - value2) )
    writeData prefix (i-1) suffix detName crd output
	

getNumber :: Char -> Int
getNumber crd
    | crd == 'z' = 10
    | crd == 'x' = 11
    | crd == 'y' = 12
    | otherwise = 0


getDetParFromStr :: String -> Char -> String -> String
getDetParFromStr detName crd str = 
    if detName `isInfixOf` str
        then if (head tuple == "det")
            then tuple !! (getNumber crd)
            else ""
        else ""
    where tuple = words str
